package Movie

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac025 on 2017/5/15.
  */
object TopRatingApp extends App{

  val conf=new SparkConf().setAppName("TopRatingApp")
    .setMaster("local[*]")
  val sc=new SparkContext(conf)

  val ratings: RDD[(Int,Double)]=sc.textFile("/Users/mac025/Downloads/ml-20m/ratings.csv")
     .map(str=>str.split(","))
       .filter(strs=>strs(1)!="movieId")
     .map(strs=>{
       (strs(1).toInt,strs(2).toDouble)
     })

  val totalRatingByMovieId=ratings.reduceByKey((acc,curr)=>acc+curr)
  totalRatingByMovieId.takeSample(false,5).foreach(println)

  val averageRatingByMovieId=ratings.mapValues(v=>v->1)
      .reduceByKey((acc,curr)=>{
        (acc._1+curr._1)->(acc._2+curr._2)
      }).mapValues(kv=>kv._1/kv._2.toDouble)

  val Top10=averageRatingByMovieId.sortBy(_._2,false).take(10)
  Top10.foreach(println)


  val movies: RDD[(Int,String)]=sc.textFile("/Users/mac025/Downloads/ml-20m/movies.csv")
    .map(str=>str.split(","))
     .filter(strs=>strs(0)!="movieId")
    //.filter(strs=>strs(2)!="genres")
    //.filter(strs=>strs(1)!="title")
    .map(strs=>strs(0).toInt->strs(1))

  val joined: RDD[(Int,(Double, String))]=averageRatingByMovieId.join(movies)
  joined.takeSample(false,5).foreach(println)
  joined.saveAsTextFile("result")


  //val res: RDD[((Int, String), Double)]=sc.textFile("result").map(str=>str.split(",")
  //    .map(strs=>strs(0).toInt->strs(1)->strs(2).toDouble)

  //ratings.take(5).foreach(println)

}
